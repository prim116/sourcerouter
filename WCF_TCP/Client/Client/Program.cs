﻿using System;

using Client.HelloService;
using System.Text;

namespace Client
{
    public class Program
    {
        public static void Main()
        {
            using (var helloService = new HelloServiceClient())
            {
                byte[] toBytes = Encoding.UTF8.GetBytes("DEVICE_ID:1;SIZE:0x84;TYPE:0x03;SPEED:60;");
                var message = helloService.GetStreamData(streamData: toBytes);
                Console.WriteLine(message);
                Console.ReadKey();
            }
        }
    }
}