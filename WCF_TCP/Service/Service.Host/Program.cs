﻿using System;
using System.ServiceModel;
using System.Reflection;
namespace Service.Host
{
    public class Program
    {
        public static void Main()
        {
            // По этому uri сервис будет доступен по протоколу net.tcp.
            var serviceNetTcpAddress = new Uri("net.tcp://localhost:8090/HelloService/");

            var serviceHost = new ServiceHost(typeof(HelloService), serviceNetTcpAddress);

            try
            {
                serviceHost.AddDefaultEndpoints();
                serviceHost.Open();
                Console.WriteLine("Запущен HOST: " + serviceHost.BaseAddresses[0].AbsoluteUri);

                //////////////////////method//////////////////////
                Console.WriteLine("\n//////////Методы Сервиса//////////");
                int count_method = 0;
                HelloService ss = new HelloService();
                Type mymethod = ss.GetType();
                MethodInfo[] methods = mymethod.GetMethods();
                foreach (MethodInfo metst in methods)
                {
                    count_method++;
                    if (metst.IsPublic)
                    {
                        Console.WriteLine("Метод(" + (count_method) + "): " + metst);
                    }
                }
                Console.WriteLine("\nМетодов сервиса: " + count_method);
                Console.WriteLine("//////////////////////////////////\n");
                //////////////////////////////////////////////////

                Console.WriteLine("Нажмите Enter чтобы остановить работу сервиса");
                // Тут программа ожидает ввода с клавиатуры, но сервис в это время запущен и доступен по uri выше.
                Console.ReadLine();

                serviceHost.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                serviceHost.Abort();
                Console.ReadLine();
            }
        }
    }
}