﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service
{
    public class Coord
    {
        public int latitude { get; set; }
        public int longitude { get; set; }
    }
}