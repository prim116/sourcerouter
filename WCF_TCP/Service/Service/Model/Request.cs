﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service
{
    public class Request
    {
        public int jsonapi { get; set; }
        public Coord coord { get; set; }
        //========CarMoveInfo=================
        public int speed { get; set; }
        public int direction { get; set; } //угол наклона относительно севера (double?)
        public int height { get; set; }
        //====================================
        public bool valid { get; set; } //признак валидности кооринат (всегда true)
        public DateTime RTime { get; set; } //время приема системой с учетом часового пояса
        public DateTime STime { get; set; }
    }
}