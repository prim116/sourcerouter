﻿using System.ServiceModel;

namespace Service.Interfaces
{
    [ServiceContract]
    public interface IHelloService
    {
        [OperationContract]
        string GetStreamData(byte[] streamData);
    }
}