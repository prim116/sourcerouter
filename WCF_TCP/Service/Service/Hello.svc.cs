﻿using System;

using Service.Interfaces;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace Service
{
    public class HelloService : IHelloService
    {
        public string GetStreamData(byte[] streamData)
        {
            string result = System.Text.Encoding.UTF8.GetString(streamData);

            string pattern = @"\SPEED.*;";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(result);
            string speed = match.Value.Replace("SPEED:","").Replace(";","");

            Request request = new Request()
            {
                jsonapi = 1,
                coord = new Coord()
                {
                    latitude = 1,
                    longitude = 2
                },
                speed = Convert.ToInt32(speed),
                direction = 1,
                height = 1,
                valid = true,
                RTime = new DateTime().Date,
                STime = new DateTime().Date
            };

            string serialized = JsonConvert.SerializeObject(request);

            return serialized;
        }
    }
}